package de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ContinuousRating extends Rating {
    private Scale scale;
    private Long defaultValue;
}
