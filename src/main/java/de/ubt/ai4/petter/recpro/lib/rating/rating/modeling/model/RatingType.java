package de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model;

public enum RatingType {
    BINARY,         // e.g. like / dislike
    CONTINUOUS,     //
    INTERVAL_BASED, // e.g. 5 Stars
    ORDINAL,        // (or CATEGORICAL) e.g. Strongly like -> Strongly dislike
    UNARY,          // only like / or dislike (?).

}
