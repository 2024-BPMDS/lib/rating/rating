package de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class BinaryRating extends Rating {
    private String trueLabel;
    private String falseLabel;
    private boolean defaultValue;
}
