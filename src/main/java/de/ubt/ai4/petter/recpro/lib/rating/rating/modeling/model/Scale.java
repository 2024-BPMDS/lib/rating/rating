package de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Scale {
    private String id;
    private List<ScaleDetail> details = new ArrayList<>();
}
